function rttov_opts_eq (opts1, opts2)
! Description:
!   Check equality of options structures
!
! Copyright:
!    This software was developed within the context of
!    the EUMETSAT Satellite Application Facility on
!    Numerical Weather Prediction (NWP SAF), under the
!    Cooperation Agreement dated 25 November 1998, between
!    EUMETSAT and the Met Office, UK, by one or more partners
!    within the NWP SAF. The partners in the NWP SAF are
!    the Met Office, ECMWF, KNMI and MeteoFrance.
!
!    Copyright 2010, EUMETSAT, All Rights Reserved.
!
! Method:
!
! Current Code Owner: SAF NWP
!
! History:
! Version   Date     Comment
! -------   ----     -------
! 2010/03 Code cleaning Pascal Brunel, Philippe Marguinaud
!
!
! Code Description:
!   Language:           Fortran 90.
!   Software Standards: "European Standards for Writing and
!     Documenting Exchangeable Fortran 90 Code".
  Use parkind1, Only : jplm
  Use rttov_types, Only : rttov_options
  Implicit None

  Logical(Kind=jplm) :: rttov_opts_eq
  
  Type(rttov_options), Intent(in) :: opts1
  Type(rttov_options), Intent(in) :: opts2

!INTF_END

! Only options meaningful for calculations
  rttov_opts_eq = &
   ( opts1%config%apply_reg_limits        .eqv. opts2%config%apply_reg_limits        ) .and. &
   ( opts1%config%do_checkinput           .eqv. opts2%config%do_checkinput           ) .and. &
   ( opts1%rt_all%switchrad               .eqv. opts2%rt_all%switchrad               ) .and. &
   ( opts1%rt_all%addrefrac               .eqv. opts2%rt_all%addrefrac               ) .and. &
   ( opts1%rt_all%use_q2m                 .eqv. opts2%rt_all%use_q2m                 ) .and. &
   ( opts1%rt_ir%addsolar                 .eqv. opts2%rt_ir%addsolar                 ) .and. &
   ( opts1%rt_ir%do_nlte_correction       .eqv. opts2%rt_ir%do_nlte_correction       ) .and. &
   ( opts1%rt_ir%addaerosl                .eqv. opts2%rt_ir%addaerosl                ) .and. &
   ( opts1%rt_ir%addclouds                .eqv. opts2%rt_ir%addclouds                ) .and. &
   ( opts1%rt_ir%user_aer_opt_param       .eqv. opts2%rt_ir%user_aer_opt_param       ) .and. &
   ( opts1%rt_ir%user_cld_opt_param       .eqv. opts2%rt_ir%user_cld_opt_param       ) .and. &
   ( opts1%rt_ir%cldstr_threshold          ==   opts2%rt_ir%cldstr_threshold         ) .and. &
   ( opts1%rt_ir%cldstr_simple            .eqv. opts2%rt_ir%cldstr_simple            ) .and. &
   ( opts1%rt_ir%ozone_data               .eqv. opts2%rt_ir%ozone_data               ) .and. &
   ( opts1%rt_ir%co2_data                 .eqv. opts2%rt_ir%co2_data                 ) .and. &
   ( opts1%rt_ir%n2o_data                 .eqv. opts2%rt_ir%n2o_data                 ) .and. &
   ( opts1%rt_ir%co_data                  .eqv. opts2%rt_ir%co_data                  ) .and. &
   ( opts1%rt_ir%ch4_data                 .eqv. opts2%rt_ir%ch4_data                 ) .and. &
   ( opts1%rt_ir%pc%addpc                 .eqv. opts2%rt_ir%pc%addpc                 ) .and. &
   ( opts1%rt_ir%pc%ipcbnd                 ==   opts2%rt_ir%pc%ipcbnd                ) .and. &
   ( opts1%rt_ir%pc%ipcreg                 ==   opts2%rt_ir%pc%ipcreg                ) .and. &
   ( opts1%rt_ir%pc%addradrec             .eqv. opts2%rt_ir%pc%addradrec             ) .and. &
   ( opts1%rt_mw%fastem_version            ==   opts2%rt_mw%fastem_version           ) .and. &
   ( opts1%rt_mw%clw_data                 .eqv. opts2%rt_mw%clw_data                 ) .and. &
   ( opts1%rt_mw%do_lambertian            .eqv. opts2%rt_mw%do_lambertian            ) .and. &
   ( opts1%interpolation%addinterp        .eqv. opts2%interpolation%addinterp        ) .and. &
   ( opts1%interpolation%interp_mode       ==   opts2%interpolation%interp_mode      ) .and. &
   ( opts1%interpolation%reg_limit_extrap .eqv. opts2%interpolation%reg_limit_extrap ) .and. &
   ( opts1%interpolation%spacetop         .eqv. opts2%interpolation%spacetop         ) .and. &
   ( opts1%interpolation%lgradp           .eqv. opts2%interpolation%lgradp           )

End function
