program rttov_coef_info
!
! Description:
!   Print out information about supplied optical depth coefficient file
!
! Copyright:
!    This software was developed within the context of
!    the EUMETSAT Satellite Application Facility on
!    Numerical Weather Prediction (NWP SAF), under the
!    Cooperation Agreement dated 25 November 1998, between
!    EUMETSAT and the Met Office, UK, by one or more partners
!    within the NWP SAF. The partners in the NWP SAF are
!    the Met Office, ECMWF, KNMI and MeteoFrance.
!
!    Copyright 2013, EUMETSAT, All Rights Reserved.
!
! Code Description:
!   Language:           Fortran 90.
!   Software Standards: "European Standards for Writing and
!     Documenting Exchangeable Fortran 90 Code".
#include "throw.h"

  use rttov_types, only : rttov_coefs, rttov_options
  use parkind1, only : jpim, jplm
  use rttov_getoptions
  use rttov_unix_env, only : rttov_exit
#ifdef _RTTOV_HDF
  use rttov_hdf_mod
#endif

  implicit none

#include "rttov_errorreport.interface"
#include "rttov_read_coefs.interface"
#include "rttov_dealloc_coefs.interface"
#include "rttov_print_info.interface"

  integer(kind=jpim)  :: err
  type(rttov_coefs)   :: coefs
  type(rttov_options) :: opts
  character(len=256)  :: f_coef = ""
  character(len=256)  :: coef_format = ""
  logical(kind=jplm)  :: verbose = .false.

  !- End of header --------------------------------------------------------
TRY

  call initoptions( "This program prints out information on RTTOV optical depth coefficient files")

  call getoption( "--coef", f_coef, MND=.true., use="coefficient file name")
#ifdef _RTTOV_HDF
  call getoption( "--format", coef_format, use="FORMATTED|UNFORMATTED|HDF5")
#else
  call getoption( "--format", coef_format, use="FORMATTED|UNFORMATTED")
#endif
  call getoption( "--verbose", verbose, use="more verbose output")
  call checkoptions()

#ifdef _RTTOV_HDF
  call open_hdf(.false., err)
  THROW(err .ne. 0)
#endif

  call rttov_read_coefs(err, coefs, opts, form_coef=coef_format, file_coef=f_coef)
  THROWM(err .ne. 0, "Failure reading coefficient file")

#ifdef _RTTOV_HDF
  call close_hdf(err)
  THROW(err .ne. 0)
#endif

  call rttov_print_info(coefs, verbose=verbose)

  call rttov_dealloc_coefs(err, coefs)
  THROWM(err .ne. 0, "Failure deallocating coefficients")

PCATCH
end program
