SUBROUTINE RTTOV_BPR_DEALLOC( ERR )
! Copyright:
!    This software was developed within the context of
!    the EUMETSAT Satellite Application Facility on
!    Numerical Weather Prediction (NWP SAF), under the
!    Cooperation Agreement dated 25 November 1998, between
!    EUMETSAT and the Met Office, UK, by one or more partners
!    within the NWP SAF. The partners in the NWP SAF are
!    the Met Office, ECMWF, KNMI and MeteoFrance.
!
!    Copyright 2012, EUMETSAT, All Rights Reserved.
!
! Current Code Owner: SAF NWP

!INTF_OFF
USE RTTOV_BPR_MOD
#include "throw.h"
!INTF_ON
USE PARKIND1,    ONLY : JPIM

IMPLICIT NONE
    INTEGER(KIND=JPIM), INTENT(OUT) :: ERR
!INTF_END

TRY

    IF (PHASE_INIT) THEN
      IF (ASSOCIATED(ARX)) DEALLOCATE(ARX, STAT = ERR)
      THROWM(err.ne.0,"Deallocation of ARX failed")
      IF (ASSOCIATED(XARR0)) DEALLOCATE(XARR0, STAT = ERR)
      THROWM(err.ne.0,"Deallocation of XARR0 failed")
      IF (ASSOCIATED(CXARR0)) DEALLOCATE(CXARR0, STAT = ERR)
      THROWM(err.ne.0,"Deallocation of CXARR0 failed")
      IF (ASSOCIATED(MUX)) DEALLOCATE(MUX, STAT = ERR)
      THROWM(err.ne.0,"Deallocation of MUX failed")

      PHASE_INIT = .FALSE.
    ENDIF

CATCH
  END SUBROUTINE RTTOV_BPR_DEALLOC
