#!/bin/sh
# Script to run the EXAMPLE_K code
#
# The result is compared to a reference file. See users guide
# to note what are the "normal" differences.
# 
# This script runs only ONE test for NOAA-16 AVHRR

# Set BIN directory if supplied
BIN=`perl -e 'for(@ARGV){m/^BIN=(\S+)$/o && print "$1";}' $*`
if [ "x$BIN" = "x" ]
then
  BIN=bin
fi

######## Edit this section for your pathnames and test case input ######

# Path relative to the rttov_test directory:
TEST=./test_example_k.1

# Paths relative to the rttov_test/${TEST} directory:
BINDIR=../../$BIN                             # BIN directory (may be set with BIN= argument)
REF_TEST=../test_example_k.2                  # Test reference data
DATART=../../rtcoef_rttov11/rttov7pred101Lgb  # Coefficients directory

# Test case input data
COEF_FILENAME="rtcoef_ground_1_hatpro.dat"
PROF_FILENAME="prof.dat"
NPROF=1         # Number of profiles defined in prof.dat
NLEVELS=51      # Number of profile levels

NCHAN=14
CHAN_LIST=$(seq -s ' ' $NCHAN)            # Space-separated channel-list

########################################################################

ARG_ARCH=`perl -e 'for(@ARGV){m/^ARCH=(\S+)$/o && print "$1";}' $*`
if [ ! "x$ARG_ARCH" = "x" ]; then
  ARCH=$ARG_ARCH
fi
if [ "x$ARCH" = "x" ];
then
  echo 'Please supply ARCH'
  exit 1
fi

CWD=`pwd`
cd $TEST

echo " "
echo " "
echo " Test K "
echo " "

echo  "Coef filename:      ${COEF_FILENAME}"
echo  "Input profile file: ${PROF_FILENAME}"
echo  "Number of profiles: ${NPROF}"
echo  "Number of levels:   ${NLEVELS}"
echo  "Number of channels: ${NCHAN}"
echo  "Channel list:       ${CHAN_LIST}"


# Coefficient file
rm -f $COEF_FILENAME
if [ -s $DATART/$COEF_FILENAME ]; then
  ln -s $DATART/$COEF_FILENAME
else
  echo "Coef file $DATART/$COEF_FILENAME not found, aborting..."
  exit 1
fi

$BINDIR/example_k.exe << EOF
${COEF_FILENAME}, Coefficient filename
${PROF_FILENAME}, Input profile filename
${NPROF}        , Number of profiles
${NLEVELS}      , Number of levels
${NCHAN}        , Number of channels
${CHAN_LIST}    , Channel list
EOF

if [ $? -ne 0 ]; then
  echo " "
  echo "TEST FAILED"
  echo " "
  exit 1
fi

OUT_FILE=output_example_k.dat
DIFF_FILE=diff_example_k.${ARCH}

mv ${OUT_FILE} ${OUT_FILE}.${ARCH}

if [ $? -ne 0 ]; then
  echo "Expected output file not found"
  exit 1
fi

echo
echo "Output is in the file ${TEST}/${OUT_FILE}.${ARCH}"

if [ -f ${REF_TEST}/${OUT_FILE} ]; then

  diff -biw ${OUT_FILE}.${ARCH} ${REF_TEST}/${OUT_FILE} > $DIFF_FILE

  if [ `stat -c %s $DIFF_FILE` -eq 0 ]; then
    echo " "
    echo "Diff file has zero size: TEST SUCCESSFUL"
    echo " "
  else
    echo "--- Diff file contents: ---"
    cat $DIFF_FILE
    echo "---------------------------"
  fi
else
  echo "Test reference output not found"
fi
echo

rm -f $COEF_FILENAME

cd $CWD

exit
