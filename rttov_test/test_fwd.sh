#!/bin/sh

# User test

# Tests forward model for a wide range of instruments.

ARG_ARCH=`perl -e 'for(@ARGV){m/^ARCH=(\S+)$/o && print "$1";}' $*`
if [ ! "x$ARG_ARCH" = "x" ]; then
  ARCH=$ARG_ARCH
fi
if [ "x$ARCH" = "x" ];
then
  echo 'Please supply ARCH'
  exit 1
fi

set -x

SESSION=test_fwd
OPTS="IGNORETINY=1 $*"
WHAT="DIRECT=1"
CHECK="CHECK=1 TEST_REF=$SESSION.2"

./rttov_test.pl SESSION=$SESSION $WHAT $CHECK ARCH=$ARCH $OPTS -- << EOF
  TEST_LIST=hatpro/021clw
  TEST_LIST=mp3000a/021clw
  TEST_LIST=tempera/001clw
  TEST_LIST=lwp_k2w/001clw
EOF
