#!/usr/bin/env python

# This script generates profile sets for the RTTOV test suite based on
# the diverse profile datasets.

# A function is required to read the profile data from the particular
# dataset and then common routines are used to write the profiles out
# possibly with modifications to certain profile parameters.

# Various additional functions are provided to interpolate profile
# sets to different levels and change specific parameters.

import os
import argparse
import numpy as np

from prof_gen import make_dirs, write_list
from prof_gen_data import p54, p44, p101gb, p150test


SURFTYPE_LAND = 0
SURFTYPE_SEA = 1

PROF_LIST = ['t', 'q', 'o3', 'co2', 'co', 'n2o', 'ch4']

MAIR = 28.9644
MH2O = 18.01528
MO3 = 47.9982

# -----------------------------------------------------------------------------
# Subroutines for reading the diverse dataset ASCII files
# -----------------------------------------------------------------------------

def skip_comment(f):
    line = f.readline()
    while line[0] == '!':
        line = f.readline()
    return line

def q2v(h2o, o3=None):
    """Convert from kg/kg to ppmv (same as rttov_q2v.F90)"""
    v_h2o = h2o / (MH2O / MAIR * (1. - h2o) + h2o)
    mwet = (1. - v_h2o) * MAIR + v_h2o * MH2O
    result = [h2o * mwet / MH2O * 1.E6]
    if o3:
        result.append(o3 * mwet / MO3 * 1.E6)
    return result

def read_div52(fname_prof, fname_surf, fname_co2='', nprof=52, nlev=101):
    """Reads the diverse 52 profile set as provided on the NWP SAF web site.
       The profile and surface data are held in separate files.
       Units for gases are kg/kg so they must be converted to ppmv.
       The CO2 data is in a separate file: it was not part of the original
       diverse 52 profile set (CO2 units assumed to be ppmv)."""

    if not os.path.exists(fname_prof):
        print 'Profile file not found'
        return
    if not os.path.exists(fname_surf):
        print 'Surface file not found'
        return

    profs = {}
    profs['nprof'] = nprof
    profs['nlev'] = nlev

    # Read profile data

    with file(fname_prof, 'r') as f:
        for p in range(nprof):
            if p == 0:
                line = skip_comment(f)
            else:
                line = f.readline()

            profs[p] = {}

            data = line.split()

            if int(data[1]) < 0:
                # Take care of the min/max/mean profiles
                profs[p]['datetime'] = [1950, 1, 1, 0, 0, 0]
                profs[p]['lat'] = 0.
                profs[p]['lon'] = 0.
                profs[p]['surftype'] = 1
            else:
                profs[p]['datetime'] = [int(data[1][0:4]), int(data[1][4:6]), int(data[1][6:8]), int(data[1][8:10]), 0, 0]
                profs[p]['lat'] = float(data[2])
                profs[p]['lon'] = float(data[3])
                profs[p]['surftype'] = SURFTYPE_LAND if float(data[5]) > 0. else SURFTYPE_SEA

            profs[p]['p2m'] = float(data[4])

            if p == 0:
                f.readline()

            keys = ['p', 't', 'q', 'o3']
            for k in keys:
                profs[p][k] = []
            for l in range(nlev):
                data = map(float, f.readline().split())
                for i, k in enumerate(keys):
                    profs[p][k].append(data[i])

            # Conversion from kg/kg to ppmv
            for l in range(nlev):
                profs[p]['q'][l], profs[p]['o3'][l] = q2v(profs[p]['q'][l], profs[p]['o3'][l])

    # Read surface variable data

    with file(fname_surf, 'r') as f:
        f.readline()

        keys = ['t2m', 'tskin', 'q2m', 'o32m', 'windu', 'windv']
        for p in range(nprof):
            data = map(float, f.readline().split())
            for i, k in enumerate(keys):
                profs[p][k] = data[i]

            # Conversion from kg/kg to ppmv
            profs[p]['q2m'], profs[p]['o32m'] = q2v(profs[p]['q2m'], profs[p]['o32m'])

    # Read CO2 data if supplied

    try:
        with file(fname_co2, 'r') as f:
            for p in range(nprof):
                if p == 0:
                    line = skip_comment(f)
                else:
                    line = f.readline()
                profs[p]['co2'] = []
                for l in range(nlev):
                    line = f.readline()
                    profs[p]['co2'].append(float(line))
    except:
        print 'No CO2 file found for div52, continuing without CO2 profiles'

    # Fill in remaining profile values with defaults (these may be overwritten later)

    for p in range(nprof):
        profs[p]['zenangle'] = 0.
        profs[p]['azangle'] = 0.
        profs[p]['sunzenangle'] = 0.
        profs[p]['sunazangle'] = 0.
        profs[p]['elevation'] = 0.
        profs[p]['be'] = 0.
        profs[p]['cosbk'] = 0.
        profs[p]['ctp'] = 500.
        profs[p]['cfraction'] = 0.
        profs[p]['idg'] = 0
        profs[p]['ish'] = 0
        profs[p]['watertype'] = 1
        profs[p]['salinity'] = 35.
        profs[p]['fastem'] = [3., 5., 15., 0.1, 0.3]
        profs[p]['windfetch'] = 100000.

    return profs


def read_div83(fname, nprof=83, nlev=101):
    """Reads the diverse 83 profile set as provided on the NWP SAF web site.
       Data for skin T and 2m T and q are not present, so these are taken
       from the bottom level of the respective atmospheric profiles.
       Units for gases are ppmv."""

    if not os.path.exists(fname):
        print 'Profile file not found'
        return

    profs = {}
    profs['nprof'] = nprof
    profs['nlev'] = nlev

    with file(fname, 'r') as f:
        line = skip_comment(f)
        for p in range(nprof):
            if p > 0:
                f.readline()
            line = f.readline()

            profs[p] = {}

            data = line.split()

            if int(data[1]) == 1000:
                # Take care of the min/max/mean profiles
                profs[p]['datetime'] = [1950, 1, 1, 0, 0, 0]
                profs[p]['lat'] = 0.
                profs[p]['lon'] = 0.
                profs[p]['surftype'] = 0
            else:
                profs[p]['datetime'] = [int(data[0]), int(data[1]), int(data[2]), 0, 0, 0]
                profs[p]['lat'] = float(data[3])
                profs[p]['lon'] = float(data[4])
                profs[p]['surftype'] = SURFTYPE_LAND if float(data[6]) >= 0.5 else SURFTYPE_SEA

            profs[p]['p2m'] = float(data[5]) # This is 1000 for min/max/mean, but that's reasonable

            keys = ['p', 't', 'q', 'co2', 'o3', 'n2o', 'co', 'ch4']
            for k in keys:
                profs[p][k] = []
            for l in range(nlev):
                data = map(float, f.readline().split())
                for i, k in enumerate(keys):
                    profs[p][k].append(data[i])

            # Take missing surface variables from the profiles

            profs[p]['t2m'] = profs[p]['t'][-1]
            profs[p]['tskin'] = profs[p]['t'][-1]
            profs[p]['q2m'] = profs[p]['q'][-1]
            profs[p]['o32m'] = profs[p]['o3'][-1]

            # Fill in remaining profile values with defaults (these may be overwritten later)

            profs[p]['windu'] = 0.
            profs[p]['windv'] = 0.

            profs[p]['zenangle'] = 0.
            profs[p]['azangle'] = 0.
            profs[p]['sunzenangle'] = 0.
            profs[p]['sunazangle'] = 0.
            profs[p]['elevation'] = 0.
            profs[p]['be'] = 0.
            profs[p]['cosbk'] = 0.
            profs[p]['ctp'] = 500.
            profs[p]['cfraction'] = 0.
            profs[p]['idg'] = 0
            profs[p]['ish'] = 0
            profs[p]['watertype'] = 1
            profs[p]['salinity'] = 35.
            profs[p]['fastem'] = [3., 5., 15., 0.1, 0.3]
            profs[p]['windfetch'] = 100000.

    return profs


# -----------------------------------------------------------------------------
# Functions for writing out test suite files
# -----------------------------------------------------------------------------

def write_common(profs, dirname, gui=False):
    """Writes the common files for all profiles in profs in directory dirname:
       atm/cloud0.txt, atm/aerosli.txt, ground/*.txt, angles.txt, be.txt, datetime.txt"""

    if gui:
        for p in range(profs['nprof']):
            d = dirname + '/{:03d}.py'.format(p + 1)
            with open(d , 'a') as f:
                f.write('\nself["CTP"] = {}'.format(profs[p]['ctp']))
                f.write('\nself["CFRACTION"] = {}'.format(profs[p]['cfraction']))

                f.write('\nself["IDG"] = {}'.format(profs[p]['idg']))
                f.write('\nself["ISH"] = {}'.format(profs[p]['ish']))

                f.write('\nself["S2M"]["T"] = {}'.format(profs[p]['t2m']))
                f.write('\nself["S2M"]["Q"] = {}'.format(profs[p]['q2m']))
                f.write('\nself["S2M"]["O"] = {}'.format(profs[p]['o32m']))
                f.write('\nself["S2M"]["P"] = {}'.format(profs[p]['p2m']))
                f.write('\nself["S2M"]["U"] = {}'.format(profs[p]['windu']))
                f.write('\nself["S2M"]["V"] = {}'.format(profs[p]['windv']))
                f.write('\nself["S2M"]["WFETC"] = {}'.format(profs[p]['windfetch']))

                f.write('\nself["SKIN"]["SURFTYPE"] = {}'.format(profs[p]['surftype']))
                f.write('\nself["SKIN"]["WATERTYPE"] = {}'.format(profs[p]['watertype']))
                f.write('\nself["SKIN"]["T"] = {}'.format(profs[p]['tskin']))
                f.write('\nself["SKIN"]["SALINITY"] = {}'.format(profs[p]['salinity']))

                f.write('\nself["ELEVATION"] = {}'.format(profs[p]['elevation']))
                f.write('\nself["ZENANGLE"] = {}'.format(profs[p]['zenangle']))
                f.write('\nself["AZANGLE"] = {}'.format(profs[p]['azangle']))
                f.write('\nself["SUNZENANGLE"] = {}'.format(profs[p]['sunzenangle']))
                f.write('\nself["SUNAZANGLE"] = {}'.format(profs[p]['sunazangle']))
                f.write('\nself["LATITUDE"] = {}'.format(profs[p]['lat']))

                f.write('\nself["BE"] = {}'.format(profs[p]['be']))
                f.write('\nself["COSBK"] = {}'.format(profs[p]['cosbk']))

            write_list(d, np.array(profs[p]['fastem'][:]), gui, 'SKIN"]["FASTEM')
            write_list(d, np.array(profs[p]['datetime'][0:3]), gui, 'DATE')
            write_list(d, np.array(profs[p]['datetime'][3:6]), gui, 'TIME')

    else:
        for p in range(profs['nprof']):
            d = dirname + '/{:03d}/atm/'.format(p + 1)
            with open(d + 'cloud0.txt', 'w') as f:
                f.write('&cloud\n')
                f.write('  ctp       = ' + str(profs[p]['ctp']) + '\n')
                f.write('  cfraction = ' + str(profs[p]['cfraction']) + '\n')
                f.write('/\n')

            with open(d + 'aerosli.txt', 'w') as f:
                f.write('&aerosli\n')
                f.write('  idg = ' + str(profs[p]['idg']) + '\n')
                f.write('  ish = ' + str(profs[p]['ish']) + '\n')
                f.write('/\n')

            d = dirname + '/{:03d}/ground/'.format(p + 1)
            with open(d + 's2m.txt', 'w') as f:
                f.write('&s2m\n')
                f.write('  s0%t     = ' + str(profs[p]['t2m']) + '\n')
                f.write('  s0%q     = ' + str(profs[p]['q2m']) + '\n')
                f.write('  s0%o     = ' + str(profs[p]['o32m']) + '\n')
                f.write('  s0%p     = ' + str(profs[p]['p2m']) + '\n')
                f.write('  s0%u     = ' + str(profs[p]['windu']) + '\n')
                f.write('  s0%v     = ' + str(profs[p]['windv']) + '\n')
                f.write('  s0%wfetc = ' + str(profs[p]['windfetch']) + '\n')
                f.write('/\n')

            with open(d + 'skin.txt', 'w') as f:
                f.write('&skin\n')
                f.write('  k0%surftype        = ' + str(profs[p]['surftype']) + '\n')
                f.write('  k0%watertype       = ' + str(profs[p]['watertype']) + '\n')
                f.write('  k0%t               = ' + str(profs[p]['tskin']) + '\n')
                f.write('  k0%salinity        = ' + str(profs[p]['salinity']) + '\n')
                f.write('  k0%fastem(1)       = ' + str(profs[p]['fastem'][0]) + '\n')
                f.write('  k0%fastem(2)       = ' + str(profs[p]['fastem'][1]) + '\n')
                f.write('  k0%fastem(3)       = ' + str(profs[p]['fastem'][2]) + '\n')
                f.write('  k0%fastem(4)       = ' + str(profs[p]['fastem'][3]) + '\n')
                f.write('  k0%fastem(5)       = ' + str(profs[p]['fastem'][4]) + '\n')
                f.write('/\n')

            with open(d + 'elevation.txt', 'w') as f:
                f.write('&elev\n')
                f.write('  elevation = ' + str(profs[p]['elevation']) + '\n')
                f.write('/\n')

            d = dirname + '/{:03d}/'.format(p + 1)
            with open(d + 'angles.txt', 'w') as f:
                f.write('&angles\n')
                f.write('  zenangle     = ' + str(profs[p]['zenangle']) + '\n')
                f.write('  azangle      = ' + str(profs[p]['azangle']) + '\n')
                f.write('  sunzenangle  = ' + str(profs[p]['sunzenangle']) + '\n')
                f.write('  sunazangle   = ' + str(profs[p]['sunazangle']) + '\n')
                f.write('  latitude     = ' + str(profs[p]['lat']) + '\n')
                f.write('/\n')

            with open(d + 'be.txt', 'w') as f:
                f.write(str(profs[p]['be']) + '   ' + str(profs[p]['cosbk']) + '\n')

            with open(d + 'datetime.txt', 'w') as f:
                f.write('   '.join(map(str, profs[p]['datetime'])) + '\n')


# =============================================================================
# Modify and write out the profile sets
# =============================================================================

# -----------------------------------------------------------------------------
# Change the zenith angle to a fixed value
# -----------------------------------------------------------------------------

def set_zenangle(profs, zen=None):
    """Set zenith angles to a new value. If zen is missing, angles cycle in
       steps of 10 degrees between 0 and 70 over the profile set."""
    if zen:
        zenlist = [zen]
    else:
        zenlist = range(0, 71, 10)

    for p in range(profs['nprof']):
        profs[p]['zenangle'] = zenlist[p % len(zenlist)]

    return profs

# -----------------------------------------------------------------------------
# Write out diverse profile set with no modifications
# -----------------------------------------------------------------------------

def make_div(dirname, profs, gui=False):
    """Write the given profiles without any modification"""

    make_dirs(dirname, profs['nprof'], gui)

    for p in range(profs['nprof']):
        if gui:
            proffn = dirname + '/{:03d}.py'.format(p + 1)
            write_list(proffn, profs[p]['p'], True, 'P')
        else:
            profdir = dirname + '/{:03d}/atm/'.format(p + 1)
            write_list(profdir + 'p.txt', profs[p]['p'])

        for v in PROF_LIST:
            if v in profs[p]:
                if gui:
                    gn = v.upper()
                    write_list(proffn, profs[p][v], gui, gn)
                else:
                    write_list(profdir + v + '.txt', profs[p][v])

    write_common(profs, dirname, gui)

# -----------------------------------------------------------------------------
# Write out diverse profile set interpolated to supplied levels
# -----------------------------------------------------------------------------

def make_div_interp(dirname, profs, pout, gui=False):
    """Interpolate each of the given profiles to the corresponding new levels
       in list p and write out."""

    make_dirs(dirname, profs['nprof'], gui)

    for p in range(profs['nprof']):
        if gui:
            proffn = dirname + '/{:03d}.py'.format(p + 1)
            write_list(proffn, pout[p], True, 'P')
        else:
            profdir = dirname + '/{:03d}/atm/'.format(p + 1)
            write_list(profdir + 'p.txt', pout[p])

        logpin = np.log(profs[p]['p'])
        logpout = np.log(pout[p])

        for v in PROF_LIST:
            if v in profs[p]:
                dout = np.interp(x=logpout, xp=logpin, fp=profs[p][v]) #, left=-999., right=-999.)
                if gui:
                    gn = v.upper()
                    write_list(proffn, dout, True, gn)
                else:
                    write_list(profdir + v + '.txt', dout)

    write_common(profs, dirname, gui)


# ---------------------------------------------------------------
# 52 diverse profiles - filenames as provided on NWP SAF web page
# ---------------------------------------------------------------

def make_div52(gui):
    # NB The CO2 profiles were not included in the original diverse 52 set
    profs = read_div52('diverse_52profiles_101L.dat', 'data52_aux.dat', 'diverse_52profiles_co2_101L.dat')

    make_div('div52', profs, gui)
    make_div_interp('div52_54L', profs, [p54] * 52, gui)
    make_div_interp('div52_101Lgb', profs, [p101gb] * 52, gui)
    make_div_interp('div52_44L', profs, [p44] * 52, gui)

    #for zen in [10., 30., 50.]:
        #profs = set_zenangle(profs, zen)
        #make_div('div52_zen' + str(int(zen)) + 'deg', profs, gui)


# -------------------------------------------------------------------------------------------
# 83 diverse profiles - filename as provided on NWP SAF web page (these are actually on 101L)
# -------------------------------------------------------------------------------------------

def make_div83(gui):
    profs = read_div83('ECMWF_83P_91L.dat')

    make_div('div83', profs, gui)
    make_div_interp('div83_54L', profs, [p54] * 83, gui)
    make_div_interp('div83_101Lgb', profs, [p101gb] * 83, gui)
    make_div_interp('div83_150L', profs, [p150test] * 83, gui)


# =============================================================================
# Call required functions
# =============================================================================

def parse_args():
    parser = argparse.ArgumentParser(description='Generate RTTOV test profiles from diverse datasets', conflict_handler='resolve')
    parser.add_argument('-g', '--gui', dest='gui', action='store_true', help='generates Python file for RTTOV_GUI')
    return parser.parse_args()

args = parse_args()
gui = args.gui

make_div52(gui)
make_div83(gui)
