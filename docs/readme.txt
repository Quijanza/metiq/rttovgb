##############################################################################
#                                                                            #
#                               RTTOV-gb v1.0                                #
#         Release Notes for RTTOV-gb version 1.0 based on RTTOV v11.2        #
#                               02 July 2018                                 #
#                                                                            #
##############################################################################

1. Licence
To use this software, users need to have registered for RTTOV-gb with the
NWP SAF (http://nwpsaf.eu/), and to have agreed to the terms of the RTTOV
license agreement.


2. Overview

RTTOV-gb is a fast one-dimensional radiative transfer model for ground-based
microwave radiometers developed at CETEMPS (De Angelis et al, 2016). The code
is based on RTTOV v11.2 and the programming interface is identical to that
version of RTTOV, though some inputs and outputs are not used by RTTOV-gb.

The RTTOV software is developed within the context of the EUMETSAT Satellite
Applications Facilty for Numerical Weather Prediction (NWP SAF). The NWP SAF
consortium comprises the Met Office, ECMWF, M�t�o France and DWD. RTTOV is
available to download for free from the NWP SAF website. Users must first
register with the site and then accept the licence agreement:
https://www.nwpsaf.eu

RTTOV-gb was developed by CETEMPS, University of L'Aquila, in the context of
the TOPROF COST Action (http://www.toprof.imaa.cnr.it).

RTTOV-gb is available for download from the NWP SAF website in the same way as
RTTOV. Users must agree to the terms of the licence. RTTOV-gb is supported by
CETEMPS and not by the NWP SAF. All questions, bug reports or requests for new
coefficients should be sent to rttovgb@aquila.infn.it.

De Angelis, F., Cimini, D., Hocking, J., Martinet, P., and Kneifel, S., 2016.
RTTOV-gb - adapting the fast radiative transfer model RTTOV for the
assimilation of ground-based microwave radiometer observations.
Geosci. Model Dev., 9, 2721-2739, doi:10.5194/gmd-9-2721-2016.


3. Installation instructions

The RTTOV-gb package is available for download from the NWP SAF website:
http://nwpsaf.eu/

After registering with the site users can add RTTOV-gb to their software
preferences and download it immediately. The RTTOV-gb package is called
rttovgb.tar.gz.

The docs/ directory contains the following files:
- a copy of this file (readme.txt)
- user guide for RTTOV-gb (users_guide_rttovgb.pdf)
- user guide for RTTOV v11.2 (users_guide_11_v1.3.pdf - NWPSAF-MO-UD-028)

New users are advised to read the RTTOV-gb user guide which gives all the
details necessary to compile and run the code.

RTTOV-gb information, bug fixes, coefficient files, and support are available
on the RTTOV-gb web site:
http://cetemps.aquila.infn.it/rttovgb/rttovgb.html


RTTOV-gb is designed for UNIX/Linux systems. The software is now successfully
tested on the following architectures and compilers: Intel systems with
gfortran, ifort, NAG, and pgf90. The compilers tested are represented by
the compiler flag files available in the build/arch/ directory.

The following system components are needed before running RTTOV-gb:
    * UNIX or Linux operating system
    * Fortran 90 compiler
    * make utilities
    * gzip and gunzip
    * about 100 MBytes of free disk space

RTTOV-gb will not work with older versions of some compilers. The following
list gives the versions of several common compilers known to be compatible:
  gfortran - v4.4.7, v7.2
  ifort - v12.0.4, v15.0.0
  NAG - v5.2, v6.1
  pgf90 - v16.1
  Cray fortran - v8.3.4

Some basic information on installing the RTTOV-gb Fortran 90 code in a UNIX or
LINUX environment follows.

------------------------------------------------------------------------------

Instructions for installing the RTTOV-gb v1.0 package obtained via the NWP SAF
helpdesk.

The file name should be rttovgb.tar.gz and this file should be copied to
the location in which you wish to install RTTOV-gb (e.g. ~user/rttovgb/).

Extract the tarball in this top-level RTTOV-gb directory:
$ tar -zxvf rttovgb.tar.gz

This creates a number of folders in the top-level directory.

The rtcoef_rttov11/rttov7pred101Lgb/ directory contains the RTTOV-gb
coefficients available at the time of release. Coefficients for other ground-
based MW sensors can be requested via the RTTOV-gb helpdesk:
rttovgb@aquila.infn.it

The web page for RTTOV-gb coefficients is:
http://cetemps.aquila.infn.it/rttovgb/rttovgb.html

------------------------------------------------------------------------------

The fortran code is organised within a number of subdirectories within
the src/ directory. The code consists of subroutine files and top 
level programs for testing the RTTOV v11 code:
- test/rttov_test.F90              Test program for RTTOV-gb (to be called via
                                   rttov_test/rttov_test.pl)
- test/example_fwd.F90             Example source code for direct model call
- test/example_k.F90               Example source code for K model call


The code may be compiled as follows. Further information can be found
in the user's guide.

From your RTTOV-gb installation directory navigate to the src/ directory.
$ cd src/

Build all RTTOV-gb libraries and executables:
$ make [ARCH=myarch]

If ARCH is omitted RTTOV-gb is compiled using gfortran. Otherwise myarch must
exist in the ../build/arch/ directory which holds files containing flags for
various compilers/architectures that have been tested by the development team.


Optional: to compile RTTOV-gb for multi-threaded execution

In order to make use of the parallel routines, RTTOV-gb must be compiled with
OpenMP. This typically involves supplying a suitable flag to an appropriate
compiler. There are compiler flag files in build/arch/ for compiling with
OpenMP support with gfortran, pgf90, ifort and NAG (v5.3 and later).


Once the code is compiled you will find bin/ and lib/ directories in your
top-level RTTOV-gb directory containing the RTTOV-gb binaries and libraries.
One library is created for each subfolder within src/ and you should link all
required libraries in your application (at the very least librttov11.2.0_main.a
and librttov11.2.0_coef_io - see the user guide for more information).

An example stand-alone Makefile src/test/Makefile_examples is included for the
example_fwd.F90 and example_k.F90 demonstration programs. This has a section
at the top which must be edited with paths appropriate to your system. These
demonstrate how to link the RTTOV libraries with separate application code.

The RTTOV-gb user guide provides details on how to test your installation, and
how to run the example code provided.

Report all bugs to the CETEMPS RTTOV-gb Helpdesk:
rttovgb@aquila.infn.it

Note: the NWP SAF does NOT provide support for RTTOV-gb.


The contents of rttovgb.tar.gz for RTTOV-gb v1.0 are:

readme.txt
docs/
docs/readme.txt
docs/rttov-test.pdf
docs/users_guide_rttovgb.pdf
docs/users_guide_11_v1.3.pdf
build/
build/Makefile.PL
build/Makefile.inc
build/Makefile.local
build/arch/
build/arch/aix
build/arch/aix-debug
build/arch/cray-ecmwf
build/arch/cray-ifort-mo
build/arch/gfortran
build/arch/gfortran-debug
build/arch/gfortran-openmp
build/arch/ifort
build/arch/ifort-debug
build/arch/ifort-mf
build/arch/ifort-openmp
build/arch/ifort-ops
build/arch/nagfor
build/arch/nagfor-debug
build/arch/nagfor-openmp
build/arch/pgf90
build/arch/pgf90-debug
build/arch/pgf90-openmp
build/cpinch.pl
build/mkintf.pl
build/mvdmod.pl
build/mypcpp.pl
data/
data/Be_LUT.2007.txt
data/asdu00
data/iasi_pc_band_2_chans.txt
data/iasi_pc_band_3_chans.txt
data/plevs.dat
data/prof.dat
data/prof_aerosl_cl.dat
data/vapo00
rtcoef_rttov11/rttov7pred101Lgb/
rtcoef_rttov11/rttov7pred101Lgb/rtcoef_ground_1_hatpro.dat
rtcoef_rttov11/rttov7pred101Lgb/rtcoef_ground_1_lwp_k2w.dat
rtcoef_rttov11/rttov7pred101Lgb/rtcoef_ground_1_mp3000a.dat
rtcoef_rttov11/rttov7pred101Lgb/rtcoef_ground_1_tempera.dat
rttov_test/profile-datasets/*
rttov_test/rttov_test.pl
rttov_test/rttov_test_plot.py
rttov_test/rttov_test_plot_mod.py
rttov_test/run_example_fwd.sh
rttov_test/run_example_k.sh
rttov_test/test_coef_io.sh
rttov_test/test_core.sh
rttov_test/test_example_fwd.1/
rttov_test/test_example_fwd.1/prof.dat
rttov_test/test_example_fwd.2/
rttov_test/test_example_fwd.2/output_example_fwd.dat
rttov_test/test_example_k.1/
rttov_test/test_example_k.1/prof.dat
rttov_test/test_example_k.2/
rttov_test/test_example_k.2/output_example_k.dat
rttov_test/test_fwd.2/*
rttov_test/test_fwd.sh
rttov_test/test_multi_instrument.2/*
rttov_test/test_multi_instrument.sh
rttov_test/test_rttovgb.2/*
rttov_test/test_rttovgb.sh
rttov_test/tests.0/*
src/
src/Makefile
src/coef_io/
src/coef_io/Makefile
src/coef_io/rttov_channel_extract_coef.F90
src/coef_io/rttov_channel_extract_pccoef.F90
src/coef_io/rttov_channel_extract_scaercoef.F90
src/coef_io/rttov_channel_extract_sccldcoef.F90
src/coef_io/rttov_check_channels_pc.F90
src/coef_io/rttov_cmpuc.F90
src/coef_io/rttov_coef_io_mod.F90
src/coef_io/rttov_coeffname.F90
src/coef_io/rttov_conv_coef.F90
src/coef_io/rttov_dealloc_coef.F90
src/coef_io/rttov_dealloc_coef_pccomp.F90
src/coef_io/rttov_dealloc_coef_scatt_ir.F90
src/coef_io/rttov_dealloc_coefs.F90
src/coef_io/rttov_dealloc_optpar_ir.F90
src/coef_io/rttov_deletecomment.F90
src/coef_io/rttov_findnextsection.F90
src/coef_io/rttov_get_pc_predictindex.F90
src/coef_io/rttov_init_coef.F90
src/coef_io/rttov_init_coef_optpar_ir.F90
src/coef_io/rttov_init_coef_pccomp.F90
src/coef_io/rttov_init_coefs.F90
src/coef_io/rttov_nullify_coef.F90
src/coef_io/rttov_nullify_coef_pccomp.F90
src/coef_io/rttov_nullify_coef_scatt_ir.F90
src/coef_io/rttov_nullify_coefs.F90
src/coef_io/rttov_nullify_optpar_ir.F90
src/coef_io/rttov_opencoeff.F90
src/coef_io/rttov_q2v.F90
src/coef_io/rttov_read_ascii_coef.F90
src/coef_io/rttov_read_ascii_pccoef.F90
src/coef_io/rttov_read_ascii_scaercoef.F90
src/coef_io/rttov_read_ascii_sccldcoef.F90
src/coef_io/rttov_read_binary_coef.F90
src/coef_io/rttov_read_binary_pccoef.F90
src/coef_io/rttov_read_binary_scaercoef.F90
src/coef_io/rttov_read_binary_sccldcoef.F90
src/coef_io/rttov_read_coefs.F90
src/coef_io/rttov_skipcommentline.F90
src/coef_io/rttov_test_get_pc_predictindex.F90
src/coef_io/rttov_v2q.F90
src/coef_io/rttov_write_ascii_coef.F90
src/coef_io/rttov_write_ascii_pccoef.F90
src/coef_io/rttov_write_ascii_scaercoef.F90
src/coef_io/rttov_write_ascii_sccldcoef.F90
src/coef_io/rttov_write_binary_coef.F90
src/coef_io/rttov_write_binary_pccoef.F90
src/coef_io/rttov_write_binary_scaercoef.F90
src/coef_io/rttov_write_binary_sccldcoef.F90
src/coef_io/rttov_write_coefs.F90
src/main/
src/main/Makefile
src/main/lapack.f
src/main/mod_rttov_baran2013_icldata.F90
src/main/mod_rttov_baran2014_icldata.F90
src/main/mod_rttov_fastem3_coef.F90
src/main/mod_rttov_fastem5_coef.F90
src/main/parkind1.F90
src/main/rttov_ad.F90
src/main/rttov_add_aux_prof.F90
src/main/rttov_add_opdp_path.F90
src/main/rttov_add_prof.F90
src/main/rttov_add_raytracing.F90
src/main/rttov_alloc_aux_prof.F90
src/main/rttov_alloc_auxrad.F90
src/main/rttov_alloc_auxrad_stream.F90
src/main/rttov_alloc_ircld.F90
src/main/rttov_alloc_opdp_path.F90
src/main/rttov_alloc_opt_param.F90
src/main/rttov_alloc_pc_dimensions.F90
src/main/rttov_alloc_pccomp.F90
src/main/rttov_alloc_predictor.F90
src/main/rttov_alloc_prof.F90
src/main/rttov_alloc_rad.F90
src/main/rttov_alloc_raytracing.F90
src/main/rttov_alloc_sunglint.F90
src/main/rttov_alloc_traj.F90
src/main/rttov_alloc_traj_dyn.F90
src/main/rttov_alloc_traj_sta.F90
src/main/rttov_alloc_trans_scatt_ir.F90
src/main/rttov_alloc_transmission.F90
src/main/rttov_alloc_transmission_aux.F90
src/main/rttov_apply_reg_limits.F90
src/main/rttov_apply_reg_limits_ad.F90
src/main/rttov_apply_reg_limits_k.F90
src/main/rttov_apply_reg_limits_tl.F90
src/main/rttov_baran2013_calc_optpar.F90
src/main/rttov_baran2013_calc_optpar_ad.F90
src/main/rttov_baran2013_calc_optpar_tl.F90
src/main/rttov_baran2014_calc_optpar.F90
src/main/rttov_baran2014_calc_optpar_ad.F90
src/main/rttov_baran2014_calc_optpar_tl.F90
src/main/rttov_baran_calc_phase.F90
src/main/rttov_baran_calc_phase_ad.F90
src/main/rttov_baran_calc_phase_tl.F90
src/main/rttov_calc_solar_spec_esd.F90
src/main/rttov_calcbt.F90
src/main/rttov_calcbt_ad.F90
src/main/rttov_calcbt_pc.F90
src/main/rttov_calcbt_pc_ad.F90
src/main/rttov_calcbt_pc_tl.F90
src/main/rttov_calcbt_tl.F90
src/main/rttov_calcemis_ir.F90
src/main/rttov_calcemis_ir_ad.F90
src/main/rttov_calcemis_ir_k.F90
src/main/rttov_calcemis_ir_tl.F90
src/main/rttov_calcemis_mw.F90
src/main/rttov_calcemis_mw_ad.F90
src/main/rttov_calcemis_mw_k.F90
src/main/rttov_calcemis_mw_tl.F90
src/main/rttov_calcrad.F90
src/main/rttov_calcrad_ad.F90
src/main/rttov_calcrad_k.F90
src/main/rttov_calcrad_tl.F90
src/main/rttov_calcsatrefl.F90
src/main/rttov_calcsatrefl_ad.F90
src/main/rttov_calcsatrefl_tl.F90
src/main/rttov_calcsurfrefl.F90
src/main/rttov_calcsurfrefl_ad.F90
src/main/rttov_calcsurfrefl_k.F90
src/main/rttov_calcsurfrefl_tl.F90
src/main/rttov_check_traj.F90
src/main/rttov_checkinput.F90
src/main/rttov_checkpcchan.F90
src/main/rttov_cldstr.F90
src/main/rttov_cldstr_ad.F90
src/main/rttov_cldstr_k.F90
src/main/rttov_cldstr_tl.F90
src/main/rttov_const.F90
src/main/rttov_copy_aux_prof.F90
src/main/rttov_copy_opdp_path.F90
src/main/rttov_copy_pccomp.F90
src/main/rttov_copy_prof.F90
src/main/rttov_copy_rad.F90
src/main/rttov_copy_raytracing.F90
src/main/rttov_direct.F90
src/main/rttov_erfcx.F90
src/main/rttov_errorhandling.F90
src/main/rttov_errorreport.F90
src/main/rttov_fastem5.F90
src/main/rttov_fastem5_ad.F90
src/main/rttov_fastem5_k.F90
src/main/rttov_fastem5_tl.F90
src/main/rttov_fresnel.F90
src/main/rttov_fresnel_ad.F90
src/main/rttov_fresnel_k.F90
src/main/rttov_fresnel_tl.F90
src/main/rttov_getoptions.F90
src/main/rttov_global.F90
src/main/rttov_init_aux_prof.F90
src/main/rttov_init_auxrad_stream.F90
src/main/rttov_init_ircld.F90
src/main/rttov_init_opdp_path.F90
src/main/rttov_init_opt_param.F90
src/main/rttov_init_pccomp.F90
src/main/rttov_init_predictor.F90
src/main/rttov_init_prof.F90
src/main/rttov_init_rad.F90
src/main/rttov_init_raytracing.F90
src/main/rttov_init_sunglint.F90
src/main/rttov_init_trans_scatt_ir.F90
src/main/rttov_init_transmission.F90
src/main/rttov_init_transmission_aux.F90
src/main/rttov_intavg_chan.F90
src/main/rttov_intavg_chan_ad.F90
src/main/rttov_intavg_chan_k.F90
src/main/rttov_intavg_chan_tl.F90
src/main/rttov_intavg_prof.F90
src/main/rttov_intavg_prof_ad.F90
src/main/rttov_intavg_prof_k.F90
src/main/rttov_intavg_prof_tl.F90
src/main/rttov_integrate.F90
src/main/rttov_integrate_ad.F90
src/main/rttov_integrate_k.F90
src/main/rttov_integrate_tl.F90
src/main/rttov_k.F90
src/main/rttov_layeravg.F90
src/main/rttov_layeravg_ad.F90
src/main/rttov_layeravg_k.F90
src/main/rttov_layeravg_tl.F90
src/main/rttov_locpat.F90
src/main/rttov_locpat_ad.F90
src/main/rttov_locpat_k.F90
src/main/rttov_locpat_tl.F90
src/main/rttov_math_mod.F90
src/main/rttov_mult_profiles_k.F90
src/main/rttov_nlte_bias_correction.F90
src/main/rttov_nlte_bias_correction_ad.F90
src/main/rttov_nlte_bias_correction_k.F90
src/main/rttov_nlte_bias_correction_tl.F90
src/main/rttov_opdep.F90
src/main/rttov_opdep_9.F90
src/main/rttov_opdep_9_ad.F90
src/main/rttov_opdep_9_k.F90
src/main/rttov_opdep_9_tl.F90
src/main/rttov_opdep_ad.F90
src/main/rttov_opdep_k.F90
src/main/rttov_opdep_tl.F90
src/main/rttov_opdpscattir.F90
src/main/rttov_opdpscattir_ad.F90
src/main/rttov_opdpscattir_k.F90
src/main/rttov_opdpscattir_tl.F90
src/main/rttov_opts_eq.F90
src/main/rttov_pcscores.F90
src/main/rttov_pcscores_ad.F90
src/main/rttov_pcscores_k.F90
src/main/rttov_pcscores_rec_k.F90
src/main/rttov_pcscores_tl.F90
src/main/rttov_profaux.F90
src/main/rttov_profaux_ad.F90
src/main/rttov_profaux_k.F90
src/main/rttov_profaux_tl.F90
src/main/rttov_reconstruct.F90
src/main/rttov_reconstruct_ad.F90
src/main/rttov_reconstruct_k.F90
src/main/rttov_reconstruct_tl.F90
src/main/rttov_refsun.F90
src/main/rttov_refsun_ad.F90
src/main/rttov_refsun_k.F90
src/main/rttov_refsun_tl.F90
src/main/rttov_setgeometry.F90
src/main/rttov_setgeometry_ad.F90
src/main/rttov_setgeometry_k.F90
src/main/rttov_setgeometry_tl.F90
src/main/rttov_setpredictors_7.F90
src/main/rttov_setpredictors_7_ad.F90
src/main/rttov_setpredictors_7_k.F90
src/main/rttov_setpredictors_7_tl.F90
src/main/rttov_setpredictors_8.F90
src/main/rttov_setpredictors_8_ad.F90
src/main/rttov_setpredictors_8_k.F90
src/main/rttov_setpredictors_8_tl.F90
src/main/rttov_setpredictors_9.F90
src/main/rttov_setpredictors_9_ad.F90
src/main/rttov_setpredictors_9_k.F90
src/main/rttov_setpredictors_9_tl.F90
src/main/rttov_sublayer.F90
src/main/rttov_sublayer_ad.F90
src/main/rttov_sublayer_k.F90
src/main/rttov_sublayer_tl.F90
src/main/rttov_tl.F90
src/main/rttov_transmit.F90
src/main/rttov_transmit_9_solar.F90
src/main/rttov_transmit_9_solar_ad.F90
src/main/rttov_transmit_9_solar_k.F90
src/main/rttov_transmit_9_solar_tl.F90
src/main/rttov_transmit_ad.F90
src/main/rttov_transmit_k.F90
src/main/rttov_transmit_tl.F90
src/main/rttov_types.F90
src/main/rttov_unix_env.F90
src/main/rttov_user_options_checkinput.F90
src/main/rttov_user_profile_checkinput.F90
src/main/throw.h
src/main/yomhook.F90
src/other/
src/other/Makefile
src/other/create_aer_clim_prof.F90
src/other/rttov_aer_clim_prof.F90
src/other/rttov_bpr_calc.F90
src/other/rttov_bpr_dealloc.F90
src/other/rttov_bpr_init.F90
src/other/rttov_bpr_mod.F90
src/other/rttov_calc_weighting_fn.F90
src/other/rttov_coef_info.F90
src/other/rttov_mie_params_aer.F90
src/other/rttov_mie_params_cld.F90
src/other/rttov_obs_to_pc.F90
src/other/rttov_print_info.F90
src/other/rttov_print_opts.F90
src/other/rttov_print_profile.F90
src/other/rttov_scattering_mod.F90
src/other/rttov_us76_prof.F90
src/other/rttov_zutility.F90
src/other/us76_to_hdf5.F90
src/parallel/
src/parallel/Makefile
src/parallel/rttov_parallel_ad.F90
src/parallel/rttov_parallel_direct.F90
src/parallel/rttov_parallel_k.F90
src/parallel/rttov_parallel_tl.F90
src/test/
src/test/Makefile
src/test/Makefile_examples
src/test/example_fwd.F90
src/test/example_k.F90
src/test/rttov_chain.F90
src/test/rttov_k_ad.F90
src/test/rttov_k_bf.F90
src/test/rttov_k_tl.F90
src/test/rttov_lun.F90
src/test/rttov_make_pccomp_inc.F90
src/test/rttov_make_profile_inc.F90
src/test/rttov_make_radiance_inc.F90
src/test/rttov_scale_pccomp_inc.F90
src/test/rttov_scale_profile_inc.F90
src/test/rttov_scale_radiance_inc.F90
src/test/rttov_test.F90
src/test/rttov_test_k_mod.F90
src/test/rttov_zutility_test.F90
src/test/test_weighting_fn_dev.F90

James Hocking, Domenico Cimini, Francesco De Angelis
02 July 2018
