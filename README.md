# RTTOV-gb

RTTOV-gb is the ground based version of RTTOV (Radiative Transfer for TOVS (TIROS Operational Vertical Sounder)) developed for the TIROS weather satellites (Television Infrared Observation Satellites) first launch in 1960.

Original code from  
http://cetemps.aquila.infn.it/rttovgb/rttovgb.html  
See  
https://www.nwpsaf.eu/site/software/rttov-gb/

# Prerequisites

From the readme:  
The following system components are needed before running RTTOV-gb
* UNIX or Linux operating system
* Fortran 90 compiler
* make utilities
* gzip and gunzip
* about 100 MBytes of free disk space"

# Build

The starting point for documentation is the supplied doc/readme.txt and doc/users_guide_rttovgb.pdf files. To build, simply have make and gfortran installed and from the src/ folder run 
```shell
make ARCH=gfortran
```

For the original source code, you can test it from rttov_test/ by running the command below. This does not work once configured for the MP3000A.

```shell
./test_rttovgb.sh ARCH=myarch
```

# Run

To run the code, go to the rttov_test/ directory and run 
```shell
./run_example_fwd.sh ARCH=gfortran
```
